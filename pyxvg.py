"""

xvg2mpl.py

Plotting XVG file from GROMACS using matplotlib.

Requires:
    * python3.9+
    * matplotlib
    * numpy
"""

import os
import re
import shlex
import sys

try:
    import numpy as np
    import matplotlib.pyplot as plt
except ImportError as e:
    print('[!] The required Python libraries could not be imported:', file=sys.stderr)
    print('\t{0}'.format(e))
    sys.exit(1)

class XVG():
    def __init__(self, xvg_path):
        self.title = "Title"
        self.labels = []
        self.units = []
        self.data = None

        self.read(xvg_path)
    
    def read(self, xvg_path):
        """Reads XVG file legends and data"""
        xvg_abspath = os.path.abspath(xvg_path)
        if not os.path.isfile(xvg_abspath):
            raise IOError(f'File not readable: {xvg_abspath}')

        _ignored = set(('legend', 'view'))
        _re_series = re.compile('s[0-9]+$')

        series = []
        num_data = []

        with open(xvg_abspath) as xvgfhandle:
            for line in xvgfhandle:
                line = line.strip()
                if line.startswith('@'):
                    tokens = shlex.split(line[1:])
                    if tokens[0] in _ignored:
                        continue
                    elif tokens[0].lower() == "title":
                        self.title = tokens[1]
                    elif tokens[0].lower() == "xaxis":
                        xaxis = tokens[2]
                    elif tokens[0].lower() == "yaxis":
                        yaxis = tokens[-1].split(",")
                    elif tokens[0] == 'TYPE':
                        if tokens[1] != 'xy':
                            raise ValueError('Graph type unsupported: \'{0}\'. Must be \'xy\''.format(tokens[1])) 
                    elif _re_series.match(tokens[0]):
                        series.append(tokens[-1])
                    else:
                        print('Unsupported entry: {token[0]} - ignoring')
                        continue
                elif line[0].isdigit():
                    num_data.append(map(float, line.split()))

        # Make labels out of metadata in xvg file
        self.labels = [ xaxis.split("(")[0].strip()] + series
        self.units = ["(" + xaxis.split("(")[1].strip()] + yaxis
        self.data = np.array(list(zip(*num_data)))
        
        #print(self.labels)
        #print(self.units)
        #print(self.data)
    def plot(self):
        """
        Plotting function.
        """
        num_plots = len(self.data)-1
        print(f"Number of plots {num_plots}.")
        fig, ax = plt.subplots(num_plots, 1)
        fig.suptitle(self.title)
        x_data = self.data[0]
        for nplot,(label,unit,data) in enumerate(zip(self.labels[1:], self.units[1:], self.data[1:])):
            print(f"plot{nplot}")
            ax[nplot].plot(x_data, data, 'o-', label=label)
            ax[nplot].set_ylabel(label + " " + unit)

        ax[-1].set_xlabel(self.labels[0] + " " + self.units[0])
        
        plt.tight_layout()
        plt.show()
                
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('xvg', type=str, help='XVG input file')
    cmd = parser.parse_args()

    xvg = XVG(cmd.xvg)
    xvg.plot()
    
